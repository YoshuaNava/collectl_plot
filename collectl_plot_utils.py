#!/usr/bin/python3

# Regular expressions
import re

# File IO
import pandas as pd

# Pretty print
import pprint as pp

# numpy
import numpy as np

# matplotlib
from matplotlib import pyplot as plt

# Iterator for cycling over color maps
from itertools import cycle


def loadExperimentMetricsFromFile(experiment_name, filepath):
  ''' Returns a dictionary containing performance metrics, generated with collectl'''

  # Validate that the specified file exists
  file = open(filepath, 'r')
  if (not file):
    print('The specified file ' + filepath + ' does not exist')
    return None

  # Regex pattern for collectl summarized performance metrics
  regex_pattern = r'\W(CPU[:\d]*|MEM|DSK|NET[:\w]*)\W(\w+\S*\w+%*)+\W?'

  # Dictionary for storing all data
  metrics = {
    'Name' : experiment_name,
    'Time' : [],
    'Date' : []
  }

  # Find line with data fields names
  matches = ()
  while(len(matches) == 0):
    line = file.readline()
    matches = re.findall(regex_pattern, line)

  # Create an empty array for each type of data
  for match in matches:
    if(match[0] not in metrics):
      metrics[match[0]] = {}
    metrics[match[0]][match[1]] = []

  # Fill dictionary with data
  eof = False
  while(not eof):
    row = file.readline()
    if (not row):
      eof = True
    else:
      row_values = row.split()
      col_index = 0
      for val in row_values:
        if(col_index == 0):
          metrics['Date'].append(val)
        elif(col_index == 1):
          metrics['Time'].append(val)
        else:
          if (not 'Name' in matches[col_index-2][1]):
            metrics[matches[col_index-2][0]][matches[col_index-2][1]].append(float(val))
        col_index += 1

  return metrics

# Reference: https://stackoverflow.com/questions/14720331/how-to-generate-random-colors-in-matplotlib
def getColorMap(n, name='hsv'):
  '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct 
  RGB color; the keyword argument name must be a standard mpl colormap name.'''
  return plt.cm.get_cmap(name, n)


def plotAgainstTime(chosen_metrics, experiments_list, folder, assume_equal_sampling=True):
  '''Plots performance metrics from experiments against time, and saves them in a provided folder'''
  color_map = getColorMap(len(experiments_list))   # Unused now, possibly useful for plotting 3+ experiments data
  if (assume_equal_sampling):
    for module, metric_list in chosen_metrics.items():
      print("Module: " + module)
      for metric, plot_settings in metric_list.items():
        # Create figure and color map
        f = plt.figure()
        cycol = cycle('bgrcmkyw')

        # Plot legends array
        plot_legends = []

        # Minimum time-points init as inf
        min_num_time_points = float('inf')

        curves_plotted = 0
        # For every experiment, find correct scale and plot points, if the desired metric was recorded
        for experiment in experiments_list:
          # Generate time points
          num_time_points = len(experiment['Time'])
          X = np.arange(num_time_points)

          # Store the minimum number of time points from any curve plotted
          if (num_time_points < min_num_time_points):
            min_num_time_points = num_time_points

          # Plot if the desired metric was recorded
          if (module in experiment) and (metric in experiment[module]):
            Y = np.array(experiment[module][metric])
            
            if ('scaling' in plot_settings):
              scaling_factor = float(plot_settings['scaling'])
              Y /= scaling_factor

            plt.plot(X, Y, linestyle='-', color=next(cycol))
            plt.hold(True)
            plot_legends.append(experiment['Name'])
            curves_plotted += 1

        # Only save graph if at least one curve was plotted
        if (curves_plotted > 0):
          y_label = module + ' ' + metric

          # Check if the user has specified units and range for the graph
          if ('units' in plot_settings):
            y_label += ' [' + plot_settings['units'] + ']'
          if ('max_y' in plot_settings):
            plt.ylim(0, plot_settings['max_y'])

          # General plot settings
          plt.xlim(0, min_num_time_points)
          plt.xlabel('Time(s)')
          plt.ylabel(y_label)
          plt.legend(plot_legends, fontsize='x-small', bbox_to_anchor=(1.3, 0.6))

          # Find the espace character and replace it
          file_name = folder + module + ' '
          if ('/' in metric):
            file_name += str.replace(metric, '/', ' per ') + '.pdf'
          else:
            file_name += metric + '.pdf'

          # Save plot as a PDF
          f.savefig(file_name, bbox_inches='tight')

          plt.close()
          print("   Metric: " + metric)
          print("     Plot saved as " + file_name)
