#!/usr/bin/python3

import collectl_plot_utils as cputils


if __name__ == "__main__":

  # General plot settings (Units, range, etc)
  plot_settings_percent = {'max_y': 100}
  plot_settings_processes = {'max_y': 50}
  plot_settings_ram = {'units': 'GB', 'max_y': 17, 'scaling': 1000000}
  plot_settings_swap = {'units': 'GB', 'max_y': 32000000}
  plot_settings_disk_io = {'units': 'MB/s', 'max_y': 200}
  plot_settings_net_io = {'units': 'MB/s', 'max_y': 100}

  # Metrics chosen for plotting
  chosen_metrics = {
    'CPU' : {
      'Sys%' : plot_settings_percent,
      'Idle%' : plot_settings_percent,
      'Sys%' : plot_settings_percent,
      'User%' : plot_settings_percent,
      'Wait%' : plot_settings_percent,
      'Totl%' : plot_settings_percent,
      'Proc/sec' : plot_settings_processes,
      'ProcQue' : {},
      'ProcRun' : plot_settings_processes,
    },
    'MEM' : {
      'Tot' : plot_settings_ram,
      'Used' : plot_settings_ram,
      'Free' : plot_settings_ram,
      'Shared' : plot_settings_ram,
      'SwapTot' : plot_settings_swap
    }
  }


  # Location and names of data files
  data_folder = '/home/ynava/anymal_workspaces/anymal_overlayed_ws/src/collectl_plot/example_data/'
  destination_folder = '/home/ynava/anymal_workspaces/anymal_overlayed_ws/src/collectl_plot/plots/'
  data_file_A = 'test_x-ynava-ThinkPad-P51-20181029.tab'
  data_file_B = 'test_y-ynava-ThinkPad-P51-20181029.tab'
  data_file_C = 'first_test-anymal-badger-npc-20181030.tab'

  experiments_list = []

  experiment_A = cputils.loadExperimentMetricsFromFile('experiment A', data_folder+data_file_A)
  experiment_B = cputils.loadExperimentMetricsFromFile('experiment B', data_folder+data_file_B)
  experiment_C = cputils.loadExperimentMetricsFromFile('experiment C', data_folder+data_file_C)

  experiments_list.append(experiment_A)
  experiments_list.append(experiment_B)
  experiments_list.append(experiment_C)


  cputils.plotAgainstTime(chosen_metrics, experiments_list, destination_folder)