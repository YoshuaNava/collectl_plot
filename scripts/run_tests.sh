#!/bin/bash

export PROC_AGG_TIME=300
export DESTINATION_FOLDER=$HOME
export TEST_FILENAME='collectl_log'
collectl -scCmnZ -i:$PROC_AGG_TIME -ozT \
         -P -f $DESTINATION_FOLDER/$TEST_FILENAME

