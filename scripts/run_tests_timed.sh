#!/bin/bash

export TIMEOUT_SIGNAL='SIGINT'
export TIMEOUT_TIME='301s'
export PROC_AGG_TIME='5'
export DESTINATION_FOLDER=$HOME
export TEST_FILENAME='collectl_log'
timeout --foreground --signal=$TIMEOUT_SIGNAL $TIMEOUT_TIME \
        collectl -scCmnZ -i:$PROC_AGG_TIME -ozT \
                -P -f $DESTINATION_FOLDER/$TEST_FILENAME

